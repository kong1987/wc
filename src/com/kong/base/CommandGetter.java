package com.kong.base;

import java.io.File;

/**
 * 
 * 
 * 获取输入的命令行，并且提取出，源操作文件，停用词文件， 指定输出文件，批量操作文件的类型
 * 
 * @author 孔绍坤
 * @version 1.0 2018/10/9
 * @since 不清楚
 *
 */
public class CommandGetter {
	private File outFile;
	private File stopFile;
	private File inputFile;
	private String fileType;
	private String[] commandStrings;

	/**
	 * 新加的
	 */

	private String fileDir;

	/**
	 * 获取输入的命令行
	 * 
	 * @param commandStrings
	 *            输入的命令行
	 */
	/*
	 * private CommandGetter() {
	 * 
	 * }
	 */
	public CommandGetter(String[] commandStrings) {
		// TODO Auto-generated constructor stub
		this.commandStrings = commandStrings;
	}

	public String getFileDir() {
		setFileDir();
		return fileDir;
	}

	/**
	 * 此处是为了配合新的需求（用户指定操作目录添加的得到文件目录的方法） 实现方法跟之前的不同，可能不够严谨
	 */
	public void setFileDir() {

		/*
		 * 添加获取command中的指定文件目录
		 */
		boolean flag = false;
		for (int i = 0; i < commandStrings.length; i++) {
			if (commandStrings[i].equals("-s")) {
				fileDir = commandStrings[i + 1];
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			fileDir = ".";
		}

	}

	/**
	 * 设置操作源文件
	 * 
	 * @return 成功读取到文件 返回true，否则返回false
	 */
	public boolean setInputFile() {
		for (String temp : commandStrings) {
			if (temp.endsWith(".java")) {
				/*
				 * 内部的判断语句用于区分输入格式为*.java以及.java
				 */
				if (!temp.equals("*.java")) {
					this.inputFile = new File(temp);
					return true;
				}
			} else {
				inputFile = null;
			}
		}
		return false;
	}

	/**
	 * 设置批量操作文件的类型
	 * 
	 * @return 成功读取到文件类型 返回true，否则返回false
	 */
	public boolean setOperateFileType() {
		for (String temp : commandStrings) {
			if (temp.startsWith("*.")) {
				String[] strs = temp.split("\\.");
				this.fileType = strs[strs.length - 1];
				return true;
			}
		}
		return false;
	}

	/**
	 * 设置输出文本文件
	 * 
	 * @return 成功读取到文件类型 返回true，否则返回false
	 */
	public boolean setOutFile() {
		for (int i = 0; i < commandStrings.length; i++) {
			if (i < commandStrings.length - 1) {
				if (commandStrings[i + 1].endsWith(".txt")
						&& commandStrings[i].equals("-o")) {
					outFile = new File(commandStrings[i + 1]);
					return true;
				} else {
					outFile = null;
				}
			}
		}
		return false;
	}

	/**
	 * 检查S命令符是否输入
	 * 
	 * @return 成功读取到S命令 返回true，否则返回false
	 */
	public boolean checkScommand() {
		for (String temp : commandStrings) {
			if (temp.equals("-s")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 检查O命令符是否输入
	 * 
	 * @return 成功读取到O命令 返回true，否则返回false
	 */
	public boolean checkOcommand() {
		for (String temp : commandStrings) {
			if (temp.equals("-o")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 设置停用词文件
	 * 
	 * @return 成功读取到停用词文件 返回true，否则返回false
	 */
	public boolean setStopFile() {
		for (int i = 0; i < commandStrings.length; i++) {
			if (i < commandStrings.length - 1) {
				if (commandStrings[i + 1].endsWith(".txt")
						&& commandStrings[i].equals("-e")) {
					stopFile = new File(commandStrings[i + 1]);
					return true;
				}
			} else {
				stopFile = null;

			}
		}
		return false;
	}

	/**
	 * 
	 * @return 获取指定输出文件
	 */
	public File getOutFile() {
		return outFile;
	}

	/**
	 * 
	 * @return 获取停用词文件
	 * 
	 */
	public File getStopFile() {
		return stopFile;
	}

	/**
	 * 
	 * @return 获取源操作文件
	 */
	public File getInputFile() {
		return inputFile;
	}

	/**
	 * 
	 * @return
	 * 
	 *         获取批量操作文件的类型
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * 
	 * @return 获取输入的命令行
	 */
	public String[] getCommandStrings() {
		return commandStrings;
	}

}
