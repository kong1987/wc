package com.kong.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * 实现WC的扩展功能：
 * 
 * @version 1.0 2018/10/9
 * @author 孔绍坤
 * @see BaseCount
 *
 */
public class ExtendCount {
	private int codeLine = -1;
	private int emptyLine = -1;
	private int commentLine = -1;
	private int afterIngoreWords = -1;
	private String[] stopWords;
	private ArrayList<File> targetFileList = new ArrayList<File>();
	private File file;
	private BaseCount baseCount;

	public ExtendCount() {

	}

	/**
	 * 
	 * @param baseCount
	 *            传入基础功能类，以便调用其方法
	 */
	public ExtendCount(BaseCount baseCount) {
		this.baseCount = baseCount;
		// TODO Auto-generated constructor stub
	}

	public int getCodeLine() {
		return codeLine;
	}

	public int getEmptyLine() {
		return emptyLine;
	}

	public int getCommentLine() {
		return commentLine;
	}

	public int getAfterIngoreWords() {
		return afterIngoreWords;
	}

	public String[] getStopWords() {
		return stopWords;
	}

	public File getFile() {
		return file;
	}

	public ArrayList<File> getTargetFileList() {
		return targetFileList;
	}

	/**
	 * 接收目标操作文件 统计文本文件中的空行数，代码行数，以及注释行数 返回总行数
	 * 
	 * @param inputFile
	 *            操作源文件
	 * @return 总行数
	 */
	public int countEveryLine(File inputFile) throws IOException {
		if (inputFile.exists()) {
			this.file = inputFile;
			FileInputStream fip = new FileInputStream(inputFile);
			InputStreamReader reader = new InputStreamReader(fip);
			StringBuffer stopSBuffer = new StringBuffer();
			while (reader.ready()) {
				stopSBuffer.append((char) reader.read());
			}
			String str = stopSBuffer.toString();
			String[] codes = str.split("\n");
			codeLine = 0;
			emptyLine = 0;
			commentLine = 0;
			for (String temp : codes) {
				if (temp.isEmpty()) {
					emptyLine++;
				} else if (temp.contains("//") || temp.contains("*/")) {
					commentLine++;
				} else {
					codeLine++;
				}
			}
			reader.close();
			fip.close();
			return codes.length;
		} else {
			return -1;
		}
	}

	/**
	 * 调用基础功能的方法，得到总的单词数，循环遍历文本文件，统计需排除单词数出现的总次数
	 * 
	 * @param stoptxt
	 *            停用词文本文件
	 * @param inputFile
	 *            操作源文件
	 * @return 返回统计排除了不统计单词后的总单词数。
	 */
	public int afterIgoreWordsCount(File stoptxt, File inputFile)
			throws IOException {
		this.file = inputFile;
		/*
		 * 调用basecount类的getw()方法获取总的单词数
		 */
		int allNumber = baseCount.getW();
		/*
		 * 获取不参与统计的字符串
		 */
		int stopWordCount = 0;
		FileInputStream fip = new FileInputStream(stoptxt);
		InputStreamReader reader = new InputStreamReader(fip);
		BufferedReader bufferedReader = new BufferedReader(reader);
		StringBuffer stopSBuffer = new StringBuffer();
		do {
			stopSBuffer.append(bufferedReader.readLine());
		} while (bufferedReader.readLine() != null);
		String allWords = stopSBuffer.toString();
		stopWords = allWords.split(" ");
		/*
		 * 计算不参与统计的单词在文本文件中出现的总次数
		 */
		FileInputStream inputFilefip = new FileInputStream(inputFile);
		InputStreamReader inputFileReader = new InputStreamReader(inputFilefip);
		BufferedReader inputBufferedReader = new BufferedReader(inputFileReader);
		StringBuffer inputSBuffer = new StringBuffer();
		do {
			inputSBuffer.append(inputBufferedReader.readLine());
		} while (inputBufferedReader.readLine() != null);

		for (String temp : stopWords) {
			int index = 0;
			while ((index = inputSBuffer.indexOf(temp, index)) != -1) {
				index = index + temp.length();
				stopWordCount++;
			}
		}

		reader.close();
		fip.close();
		inputFileReader.close();
		inputFilefip.close();

		/*
		 * 返回计算后的单词数
		 */
		afterIngoreWords = allNumber - stopWordCount;
		return afterIngoreWords;
	}

	/**
	 * 获取指定目录下的以及子目录下的所有文件,,并从中提取出目标文件
	 * 
	 * @param 指定目录路径
	 * @param 批量操作文件的类型
	 */
	private void setFileList(String filePath, String fileType) {
		File file = new File(filePath);
		if (file.exists()) {
			if (file.isDirectory()) {
				File[] files = file.listFiles();
				// 遍历目录下的所有文件

				if (files.length != 0) {
					for (File f : files) {
						if (f.isFile()) {
							if (f.getName().endsWith(fileType)) {
								targetFileList.add(f);
							}
						} else if (f.isDirectory()) {
							setFileList(f.getAbsolutePath(), fileType);
						}
					}
				}
			}
		} else {
			System.out.println("文件路径不对！");
		}
	}

	/**
	 * 调用setFileList()
	 * 
	 * @param fileType
	 *            目标文件类型
	 * @param filePath
	 *            目录路径
	 * @return 如果目标文件表中有文件，返回true，否则返回false
	 * 
	 */
	public boolean setTargetFileList(String fileType, String filePath) {
		setFileList(filePath, fileType);
		if (targetFileList.size() == 0) {
			return false;
		} else {
			return true;
		}
	}
}
