package com.kong.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 主类：进行整体逻辑的处理
 * 
 * @version 1.0 2018/10/9
 * @author 孔绍坤
 *
 */
public class Main {
	public static File outFile;
	public static File stopFile;
	public static File inputFile;

	/*
	 * 用于标记是否输入了相关文件
	 */
	public static boolean inputFileFlag = false;
	public static boolean outFileFlag = false;
	public static boolean stopFileFlag = false;
	public static boolean operateFileTypeFlag = false;

	/*
	 * 相关功能类
	 */

	public static BaseCount baseCount = new BaseCount();;
	public static ExtendCount extendCount = new ExtendCount(baseCount);
	public static FileOutputer fileOutputer = new FileOutputer(baseCount,
			extendCount);
	public static CommandGetter commandGetter;
	
	public static String[] commands;
	
	public Main(String[] commands) {
		// TODO Auto-generated constructor stub
		this.commands=commands;
	}

	public static void originalMain() {
		commandGetter = new CommandGetter(commands);

		/*
		 * 获取是否输入相应的文件
		 */
		inputFileFlag = commandGetter.setInputFile();// 标识是否输入源操作文件
		outFileFlag = commandGetter.setOutFile();// 标识是否指定输出文本文件
		stopFileFlag = commandGetter.setStopFile();// 标识是否输入停用词文件
		operateFileTypeFlag = commandGetter.setOperateFileType();// 标识是否输入批量操作文件的类型

		/*
		 * 得到相关文件
		 */
		outFile = commandGetter.getOutFile();
		inputFile = commandGetter.getInputFile();
		stopFile = commandGetter.getStopFile();

		try {
			if (inputFile.exists()) {
				chooiseCommand(inputFile);
			} else {
				System.out.println("输入的操作源文件不存在！");
			}
			manageScommand();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * 管理基本的操纵命令，根据不同的命令调用不同的方法
	 * 
	 * @param targetFile
	 *            操作源文件
	 * @throws IOException
	 */
	public static void chooiseCommand(File targetFile) throws IOException {

		if (inputFileFlag) {
			for (String str : commandGetter.getCommandStrings()) {
				switch (str) {
				case "-c":
					baseCount.countChar(targetFile);
					break;
				case "-l":
					baseCount.countLine(targetFile);
					break;
				case "-w":
					baseCount.countWord(targetFile);
					break;
				case "-a":
					extendCount.countEveryLine(targetFile);
					// 加入统计代码行数、空行数、注释行数的方法
					break;
				case "-e":
					if (stopFileFlag) {
						if (stopFile.exists()) {
							extendCount.afterIgoreWordsCount(stopFile,
									targetFile);
						} else {
							System.out.println("输入的停用词文件不存在！");
						}
					} else {
						System.out
								.println("-e 必须与停用词文件名同时使用，且停用词文件必须紧跟在-e参数后面，不允许单独使用-e参数。");
					}
					break;
				default:
					break;
				}
			}
			manageOcommand();

		} else {
			System.out.println("请输入源文件");
		}
	}

	/**
	 * 
	 * 判断命令行中是否存在输出指令以及指定输出的文本文件，并作出相应的回应
	 * 
	 * @throws IOException
	 */
	public static void manageOcommand() throws IOException {
		if (commandGetter.checkOcommand()) {
			if (outFileFlag) {
				fileOutputer.outputFile(outFile);
			} else {
				System.out
						.println("-o 必须与文件名同时使用，且输出文件必须紧跟在-o参数后面，不允许单独使用-o参数!");
			}
		} else {
			fileOutputer.outputFile(new File("result.txt"));
		}
	}

	/**
	 * 判断输入的命令行中是否存在递归处理该目录下相关文件的指令，并作出相应的回应
	 *
	 * @throws IOException
	 */
	public static void manageScommand() throws IOException {
		boolean sCommandExist = commandGetter.checkScommand();
		if (sCommandExist) {
			if (operateFileTypeFlag) {
				// 加入递归处理文件的方法 String fileType

				ArrayList<File> files;
				String fileType = commandGetter.getFileType();
				String fileDir=commandGetter.getFileDir();
				if (extendCount.setTargetFileList(fileType, fileDir)) {
					files = extendCount.getTargetFileList();
					for (File f : files) {
						chooiseCommand(f);
					}
				} else {
					System.out.println("该类型文件在本目录下不存在！");
				}

			} else {
				System.out.println("请输入要批量处理的文件格式！");
			}
		} else if (operateFileTypeFlag) {
			System.out.println("请指定-S操作符！");
		}
	}
}