package com.kong.base;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

public class FrameWork {
	public static Main mainTwo;// 用于连接原本实现的代码

	public static void main(String[] args) {

		/* 主界面 */
		final JFrame frame = new JFrame("WordCount");// 软件左上角名称，新建一个界面

		frame.setSize(400, 300);// 500为长，300为宽
		frame.setLocation(400, 300);// 为出现在屏幕上位置
		frame.setLayout(null);

		/* 面板一 */
		JPanel j1 = new JPanel();// 新建一个面板
		j1.setBounds(50, 50, 300, 60);// 一二参数：位置，三四参数：填充大小
		j1.setBackground(Color.white); // 设置面板背景颜色

		j1.setLayout(new FlowLayout());
		/* 组件:复选框 */
		JLabel jfunctionView = new JLabel("     程序功能");
		final JCheckBox jcb1 = new JCheckBox("行数统计");
		final JCheckBox jcb2 = new JCheckBox("字符统计");
		final JCheckBox jcb3 = new JCheckBox("单词统计");
		final JCheckBox jcb4 = new JCheckBox("代码行/注释行/空行");
		final JCheckBox jcb5 = new JCheckBox("批量操作");
		jcb1.setSelected(false);
		jcb2.setSelected(false);
		jcb3.setSelected(false);
		jcb4.setSelected(false);
		jcb5.setSelected(false);
		j1.add(jfunctionView);
		j1.add(jcb1);
		j1.add(jcb2);
		j1.add(jcb3);
		j1.add(jcb4);
		j1.add(jcb5);

		/**
		 * 面板二***********************************************
		 * 
		 * */
		JPanel j2 = new JPanel();
		j2.setBounds(10, 150, 300, 60);
		j2.setBackground(Color.lightGray);
		// j2.setLayout(null);

		/* 组件:文件选择器 */
		final JFileChooser fc = new JFileChooser();// 文件选择器，用于基础功能实现
		final JFileChooser pc = new JFileChooser();// 目录选择器，用于批量操作实现
		final JFileChooser sc = new JFileChooser();// 文件选择器，用于保存文件实现
		pc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);// 设置只能选择目录
		/* 文件类型过滤器 */
		fc.setFileFilter(new FileFilter() {

			/* 要求只接受.java文件 */
			@Override
			public String getDescription() {
				// TODO Auto-generated method stub
				return ".java";
			}

			@Override
			public boolean accept(File f) {
				// TODO Auto-generated method stub
				return f.getName().toLowerCase().endsWith(".java");
			}
		});

		JButton bOpen = new JButton("选择源码文件");
		JButton bSave = new JButton("选择输出文件");
		JButton bStop = new JButton("选择停止词文件");
		final JTextField jFileType = new JTextField("");
		jFileType.addFocusListener(new JTextFieldHintListener(jFileType,
				"请输入文件类型,如\"*.java\""));
		jFileType.setPreferredSize(new Dimension(150, 30));

		// jFileType.setEnabled(false);

		JButton bPath = new JButton("选择文件目录");
		// bPath.setEnabled(false);
		JButton jsbumit = new JButton("提交");
		jsbumit.setPreferredSize(new Dimension(120, 30));

		// 把按钮加入面板
		j2.add(bOpen);
		j2.add(bSave);
		j2.add(bStop);
		j2.add(jFileType);
		j2.add(bPath);
		j2.add(jsbumit);

		final CommandSetter tempcs = new CommandSetter();

		/* 为打开文件按钮添加点击事件 */
		bOpen.addActionListener(new ActionListener() {
			public String filePath;

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int retVal = fc.showOpenDialog(frame);
				File file = fc.getSelectedFile();
				if (retVal == JFileChooser.APPROVE_OPTION) {
					JOptionPane.showMessageDialog(frame,
							"你选择了文件:" + file.getAbsolutePath());

					filePath = file.getAbsolutePath();
					tempcs.setSourceFile(filePath);
				}
			}

		});

		/* 为打开文件目录按钮添加点击事件 */
		bPath.addActionListener(new ActionListener() {
			public String filePath;

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int retVal = pc.showOpenDialog(frame);
				File file = pc.getSelectedFile();
				if (retVal == JFileChooser.APPROVE_OPTION) {
					JOptionPane.showMessageDialog(frame,
							"你选择了文件夹:" + file.getAbsolutePath());
					filePath = file.getAbsolutePath();
					tempcs.setFilePath(filePath);
				}
			}
		});

		bSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int option = sc.showSaveDialog(null);
				if (option == JFileChooser.APPROVE_OPTION) {
					File file = sc.getSelectedFile();
					FileOutputStream fos;
					JOptionPane.showMessageDialog(frame,
							"保存文件路径为:" + file.getAbsolutePath());
					tempcs.setOutFile(file.getAbsolutePath());
					tempcs.setComO("-o");
					try {
						fos = new FileOutputStream(file);
						fos.close();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});

		/* 为选择停止词文件添加点击事件 */
		bStop.addActionListener(new ActionListener() {
			public String filePath;

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int retVal = fc.showOpenDialog(frame);
				File file = fc.getSelectedFile();
				if (retVal == JFileChooser.APPROVE_OPTION) {
					tempcs.setComE("-e");
					JOptionPane.showMessageDialog(frame,
							"停止文件为:" + file.getAbsolutePath());
					filePath = file.getAbsolutePath();
					tempcs.setStopFile(filePath);
				}
			}
		});

		/* 为提交按钮添加点击事件 */
		jsbumit.addActionListener(new ActionListener() {

			CommandSetter cSet = new CommandSetter();

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (jcb1.isSelected()) {
					cSet.setComLine("-l");
				}
				if (jcb2.isSelected()) {
					cSet.setComChar("-c");
				}
				if (jcb3.isSelected()) {
					cSet.setComWord("-w");
				}
				if (jcb4.isSelected()) {
					cSet.setComAdva("-a");
				}
				if (jcb5.isSelected()) {
					cSet.setComBatch("-s");
					// bPath.setEnabled(true);
				}

				/* 获得输入框文件类型 */
				String fileType = jFileType.getText();

				if (fileType.startsWith("*")) {
					cSet.setFileType(fileType);
				} else {
					JOptionPane.showMessageDialog(frame,
							"对不起，你输入的不是一个合法的文件类型，请以\"*\"开始");
				}
				cSet.setSourceFile(tempcs.getSourceFile());
				cSet.setOutFile(tempcs.getOutFile());
				cSet.setFilePath(tempcs.getFilePath());
				cSet.setStopFile(tempcs.getStopFile());
				cSet.setComE(tempcs.getComE());
				cSet.setComO(tempcs.getComO());
				Main mainTwo = new Main(cSet.getCommand());
				mainTwo.originalMain();// 用于连接原本实现的代码
				JOptionPane.showMessageDialog(frame, "操作成功！青岛相关目录下查看！");
			}
		});

		/* 设置双界面格式 */
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, j1, j2);
		sp.setDividerLocation(150);
		frame.setContentPane(sp);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);// 设置可见度显示
	}
}

/* 为输入框焦点事件添加逻辑 */
class JTextFieldHintListener implements FocusListener {
	private String hintText;
	private JTextField textField;

	public JTextFieldHintListener(JTextField jTextField, String hintText) {
		this.textField = jTextField;
		this.hintText = hintText;
		jTextField.setText(hintText); // 默认直接显示
		jTextField.setForeground(Color.GRAY);
	}

	@Override
	public void focusGained(FocusEvent e) {
		// 获取焦点时，清空提示内容
		String temp = textField.getText();
		if (temp.equals(hintText)) {
			textField.setText("");
			textField.setForeground(Color.BLACK);
		}

	}

	@Override
	public void focusLost(FocusEvent e) {
		// 失去焦点时，没有输入内容，显示提示内容
		String temp = textField.getText();
		if (temp.equals("")) {
			textField.setForeground(Color.GRAY);
			textField.setText(hintText);
		}

	}

}
