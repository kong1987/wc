package com.kong.base;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * 文件输出类，处理输出信息，输出信息到文件
 * 
 * @author 孔绍坤
 * @version 1.0 2018/10/9
 *
 */
public class FileOutputer {

	private BaseCount baseCount;
	private ExtendCount extendCount;

	/**
	 * 
	 * @param baseCount
	 *            基础功能类
	 * @param extendCount
	 *            扩展功能类
	 */
	public FileOutputer(BaseCount baseCount, ExtendCount extendCount) {
		this.baseCount = baseCount;
		this.extendCount = extendCount;
		// TODO Auto-generated constructor stub
	}

	/**
	 * 判断输入的操作源文件是否存在
	 * 
	 * @return 如果存在，则返回true否则返回false
	 */
	public boolean checkInputFileExist() {
		if (baseCount.getFile().exists()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * 将统计结果输出到文本文件中 输出详细的文本文件的统计结果
	 * 
	 * @see com.kong.base.BaseCount
	 * @see ExtendCount
	 * 
	 * @param outputFile
	 *            输出文本文件
	 * @throws IOException
	 */
	public void outputFile(File outputFile) throws IOException {
		boolean inputFileExist = checkInputFileExist();
		if (inputFileExist) {
			File f = outputFile;
			FileOutputStream fop = new FileOutputStream(f, true);
			OutputStreamWriter writer = new OutputStreamWriter(fop);
			if (baseCount.getC() >= 0) {

				writer.append(baseCount.getFile().getName() + ",总字符数："
						+ baseCount.getC() + "\r\n");
			}
			if (baseCount.getW() >= 0) {
				writer.append(baseCount.getFile().getName() + ",总单词数："
						+ baseCount.getW() + "\r\n");
			}
			if (baseCount.getL() >= 0) {
				writer.append(baseCount.getFile().getName() + ",总行数："
						+ baseCount.getL() + "\r\n");
			}
			if (extendCount.getCodeLine() >= 0) {
				writer.append(extendCount.getFile().getName()
						+ ",代码行数\\空行数\\注释行数：" + extendCount.getCodeLine()
						+ "\\" + extendCount.getEmptyLine() + "\\"
						+ extendCount.getCommentLine() + "\r\n");
			}
			if (extendCount.getAfterIngoreWords() >= 0) {
				writer.append(extendCount.getFile().getName() + ",不统计的单词有：");
				for (String temp : extendCount.getStopWords()) {
					writer.append(temp + " ");
				}
				writer.append("结果单词数：" + extendCount.getAfterIngoreWords()
						+ "\r\n");
			}
			writer.close();
			fop.close();
		}
	}
}
