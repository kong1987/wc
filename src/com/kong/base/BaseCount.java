package com.kong.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * 实现WC的基础功能，统计字符数，单词数，行数
 * 
 * @author 孔绍坤
 * @version 1.0
 * @since 最低的JDK版本
 * 
 *
 */
public class BaseCount {
	private static int c = -1;
	private static int w = -1;
	private static int l = -1;
	private static File file;

	/**
	 * 
	 * 统计总的字符数
	 * 
	 * @param inputFile
	 *            指定的目标操作文件
	 * @return c 返回统计的字符数
	 * @throws IOException
	 */
	public int countChar(File inputFile) throws IOException {
		file = inputFile;
		if (inputFile.exists()) {
			FileInputStream fip = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(fip);
			StringBuffer sBuffer = new StringBuffer();
			while (reader.ready()) {
				sBuffer.append((char) reader.read());
			}

			c = sBuffer.length();
			reader.close();
			fip.close();
			return c;
		} else {
			System.out.println("统计字符：输入的源文件不存在！");
			return -1;
		}
	}

	/**
	 * 统计总的行数
	 * 
	 * @param inputFile
	 *            接收一个目标操作文件
	 * @return l 返回统计的行数
	 */
	public int countLine(File inputFile) throws IOException {
		if (inputFile.exists()) {
			file = inputFile;
			FileInputStream fip = new FileInputStream(file);
			InputStreamReader reader = new InputStreamReader(fip);
			StringBuffer sBuffer = new StringBuffer();
			while (reader.ready()) {
				sBuffer.append((char) reader.read());
			}
			String str = sBuffer.toString();
			char[] ch = str.toCharArray();
			l = 0;
			for (char c : ch) {
				if (c == '\n') {
					l++;
				}
			}
			reader.close();
			fip.close();
			return l;
		} else {
			System.out.println("统计行数：输入的源文件不存在！");
			return -1;
			
		}

	}

	/**
	 * 统计总的单词数
	 * 
	 * @param inputFile
	 *            接收一个目标操作文件
	 * 
	 * @return w 返回统计的单词数
	 */
	public int countWord(File inputFile) throws IOException {
		if(inputFile.exists())
		{
		file = inputFile;
		FileInputStream fip = new FileInputStream(file);
		InputStreamReader reader = new InputStreamReader(fip);
		StringBuffer sBuffer = new StringBuffer();
		while (reader.ready()) {
			sBuffer.append((char) reader.read());
		}
		String str = sBuffer.toString();
		char[] ch = str.toCharArray();
		w = 0;
		for (char c : ch) {
			if (c == '\n' || c == '\r' || c == ' ' || c == ',' || c == ';'
					|| c == '.') {
				w++;
			}
		}
		reader.close();
		fip.close();
		return w;
		}else {
			System.out.println("统计单词数：输入的源文件不存在！");
			return -1;
		}
	}

	public int getC() {
		return c;
	}

	public int getW() {
		return w;
	}

	public int getL() {
		return l;
	}
	
	public File getFile()
	{
		return file;
	}
}
