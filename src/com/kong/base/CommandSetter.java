package com.kong.base;


import java.util.List;
import java.util.ArrayList;

public class CommandSetter {

	private String comLine;//行数命令 -l
	private String comChar;//字符数命令 -c
	private String comWord;//单词数命令 -w
	private String comAdva;//扩展功能，空行/注释行/代码行 -a
 	private String comBatch;//批量操作命令 -s
 	private String comO;//输出文件命令-o
 	private String comE;//停用词命令-e
	
	private String sourceFile;//源文件目录
	private String filePath;//选择批量操作文件主目录
	private String outFile;//输出文件目录
	private String stopFile;//停止文件目录 
	private String fileType;//文件类型 *.java   *.c    *.txt
	
private List<String> commandSet=new ArrayList<String>();
	
	private void setCommand() {
		
		commandSet.add(comLine);
		commandSet.add(comChar);
		commandSet.add(comWord);
		commandSet.add(comAdva);
		commandSet.add(comBatch);
		commandSet.add(filePath);//写入时固定将目录路径写在-s后面
		commandSet.add(fileType);
		commandSet.add(sourceFile);
		
		commandSet.add(comE);
		commandSet.add(stopFile);
		commandSet.add(comO);
		commandSet.add(outFile);
		
		
	}

	public String[] getCommand()
	{
		setCommand();
		return (String[])commandSet.toArray(new String[0]);
		
	}

	
	public String getComO() {
		return comO;
	}
	public void setComO(String comO) {
		this.comO = comO;
	}
	public String getComE() {
		return comE;
	}
	public void setComE(String comE) {
		this.comE = comE;
	}
	public String getComLine() {
		return comLine;
	}
	public void setComLine(String comLine) {
		this.comLine = comLine;
	}
	public String getComChar() {
		return comChar;
	}
	public void setComChar(String comChar) {
		this.comChar = comChar;
	}
	public String getComWord() {
		return comWord;
	}
	public void setComWord(String comWord) {
		this.comWord = comWord;
	}
	public String getComAdva() {
		return comAdva;
	}
	public void setComAdva(String comAdva) {
		this.comAdva = comAdva;
	}
	public String getComBatch() {
		return comBatch;
	}
	public void setComBatch(String comBatch) {
		this.comBatch = comBatch;
	}
	public String getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getOutFile() {
		return outFile;
	}
	public void setOutFile(String outFile) {
		this.outFile = outFile;
	}
	public String getStopFile() {
		return stopFile;
	}
	public void setStopFile(String stopFile) {
		this.stopFile = stopFile;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

}
