package com.kong.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class BaseCount {
	private int c = -1;
	private int w = -1;
	private int l = -1;
	private File file;

	public int countChar(String file) throws IOException {
		this.file = new File(file);
		FileInputStream fip = new FileInputStream(this.file);
		InputStreamReader reader = new InputStreamReader(fip);
		StringBuffer sBuffer = new StringBuffer(); 
		while (reader.ready()) {
			sBuffer.append((char) reader.read());
		}

		c = sBuffer.length();
		reader.close();
		fip.close();
		return c;
	}

	public int countLine(String file) throws IOException {
		this.file = new File(file);
		FileInputStream fip = new FileInputStream(this.file);
		InputStreamReader reader = new InputStreamReader(fip);
		StringBuffer sBuffer = new StringBuffer();
		while (reader.ready()) {
			sBuffer.append((char) reader.read());
		}
		String str = sBuffer.toString();
		char[] ch = str.toCharArray();
		l = 0;
		for (char c : ch) {
			if (c == '\n') {
				l++;
			}
		}
		reader.close();
		fip.close();
		return l;

	}

	public int countWord(String file) throws IOException {
		this.file = new File(file);
		FileInputStream fip = new FileInputStream(this.file);
		InputStreamReader reader = new InputStreamReader(fip);
		StringBuffer sBuffer = new StringBuffer();
		while (reader.ready()) {
			sBuffer.append((char) reader.read());
		}
		String str = sBuffer.toString();
		char[] ch = str.toCharArray();
		w = 0;
		for (char c : ch) {
			if (c == '\n' || c == '\r' || c == ' ' || c == ',' || c == ';' || c=='.') {
				w++;
			}
		}
		reader.close();
		fip.close();
		return w;
	}

	public void outputFile(String file) throws IOException {
		File f = new File(file);
		FileOutputStream fop = new FileOutputStream(f);
		OutputStreamWriter writer = new OutputStreamWriter(fop);
		if (c > 0) {
			writer.append(this.file.getName() + ",字符数：" + c+"\r\n");
		}
		if (w > 0) {
			writer.append(this.file.getName() + ",单词数：" + w+"\r\n");
		}
		if (l > 0) {
			writer.append(this.file.getName() + ",行数：" + l+"\r\n");
		}
		writer.close();
		fop.close();
	}

}
