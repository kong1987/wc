package com.kong.base;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * 
    用 @RunWith(Parameterized.class) 来注释 test 类。
    创建一个由 @Parameters 注释的公共的静态方法，它返回一个对象的集合(数组)来作为测试数据集合。
    创建一个公共的构造函数，它接受和一行测试数据相等同的东西。
    为每一列测试数据创建一个实例变量。
    用实例变量作为测试数据的来源来创建你的测试用例。

 * @author 孔绍坤
 *
 */
@RunWith(Parameterized.class)
public class CommandGetterTest {
	private String[] commands;
	private String[] expectedResult;
	private CommandGetter commandGetter;
	
	/*
	 *特备说明：expectedResult存数的都boolean的数据，为了使所有的方法都可以同时进行参数化测试
	 */
	@Before
	public void Init()
	{
		commandGetter=new CommandGetter(commands);
	}
	
	public CommandGetterTest(String[] commands,String[] expectedResult) {
		this.commands=commands;
		this.expectedResult=expectedResult;
		// TODO Auto-generated constructor stub
	}
	
	@Parameters
	public static Collection primeData() {
		
		//模拟命令
		String[] str1={"test.java","-o","-s"};
		String[] str2={"-c","-l","-w","test1.java","-o","out1.txt"};
		String[] str3={"-o","out.txt","test.java","*.c","-s","-e","stop.txt"};
		String[] str4={"*.c","-s","-e"};
		
		//调用的方法返回值集合
		String[] bool1={"true","false","false","true","true","false"};
		String[] bool2={"true","false","true","false","true","false"};
		String[] bool3={"true","true","true","true","true","true"};
		String[] bool4={"false","true","false","true","false","false"};
		Object[][] object=new Object[][]{
				{str1,bool1},
				{str2,bool2},
				{str3,bool3},
				{str4,bool4}
				};
		return Arrays.asList(object);
		}


	@Test
	public void testSetInputFile() {
		System.out.println("This is testSetInputFile :");
		assertEquals(expectedResult[0], ((Boolean)commandGetter.setInputFile()).toString());
	}
	
	@Test
	public void testSetOperateFileType() {
		System.out.println("This is testSetOperateFileType :");
		assertEquals(expectedResult[1], ((Boolean)commandGetter.setOperateFileType()).toString());
	}


	@Test
	public void testSetOutFile() {
		System.out.println("This is testSetOutFile :");
		assertEquals(expectedResult[2], ((Boolean)commandGetter.setOutFile()).toString());
	}

	@Test
	public void testCheckScommand() {
		System.out.println("This is testCheckScommand :");
		assertEquals(expectedResult[3], ((Boolean)commandGetter.checkScommand()).toString());
	}

	@Test
	public void testCheckOcommand() {
		System.out.println("This is testCheckOcommand :");
		assertEquals(expectedResult[4], ((Boolean)commandGetter.checkOcommand()).toString());
	}

	
	@Test
	public void testSetStopFile() {
		System.out.println("This is testSetStopFile :");
		assertEquals(expectedResult[5], ((Boolean)commandGetter.setStopFile()).toString());
	}

}
