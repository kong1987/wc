package com.kong.base;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class ExtendCountTest {
	public static File inputFile;
	public static File stopFile;
	public static ExtendCount extendCount;
	public static BaseCount baseCount;

	@BeforeClass
	public static void InitExtend() {
		inputFile=new File("test1.java");
		stopFile=new File("stop.txt");
		baseCount=new BaseCount();
		extendCount=new ExtendCount(baseCount);
	}

	@Test
	public void testCountEveryLine() throws IOException {
		
		int l=extendCount.countEveryLine(inputFile);
		assertTrue(l>0);
		System.out.println("this is EX");
		
	}

	@Test
	public void testAfterIgoreWordsCount() throws IOException {	
		int w=extendCount.afterIgoreWordsCount(stopFile, inputFile);
		assertTrue(w<0);
		
	}

	@Test
	public void testSetTargetFileList() {
		boolean flag=extendCount.setTargetFileList("java", ".");
		assertTrue(flag);
	}
	@Ignore
	@Test
	public void testGetCodeLine() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetEmptyLine() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetCommentLine() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetAfterIngoreWords() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetStopWords() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetFile() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testGetTargetFileList() {
		fail("Not yet implemented");
	}
	
	@AfterClass
	public static void end()
	{
		inputFile=null;
		stopFile=null;
		baseCount=null;
		extendCount=null;
	}

}
