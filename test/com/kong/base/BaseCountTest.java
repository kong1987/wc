package com.kong.base;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class BaseCountTest {
	public static BaseCount baseCount = new BaseCount();
	public static File file1;
	public static File file2;

	@BeforeClass
	public static void initBaseCount() throws IOException {

		file1 = new File("test1.java");
		file2 = new File("test.java");

	}

	@Test
	public void testCountChar() throws IOException {

		int c = baseCount.countChar(file1);
		assertTrue(c > 0);

	}

	
	@Test
	public void testCountChar1() throws IOException {

		int c = baseCount.countChar(file2);
		assertTrue(c < 0);
		assertEquals(c, -1);
	}

	@Test
	public void testCountLine() throws IOException {
		int l=baseCount.countLine(file1);
		assertTrue(l>0);
	}
	
	@Test
	public void testCountLine1() throws IOException {
		int l=baseCount.countLine(file2);
		assertTrue(l<0);
		assertEquals(l, -1);
	}

	
	@Test
	public void testCountWord() throws IOException {
		int w=baseCount.countWord(file1);
		assertTrue(w>0);
	}
	@Test
	public void testCountWord1() throws IOException {
		int w=baseCount.countWord(file2);
		assertTrue(w<0);
		assertEquals(w, -1);
	}



	@Test
	public void testGetC() throws IOException {
		testCountChar();
		assertTrue(baseCount.getC()>0);
	}
	
	@Test
	public void testGetC1() throws IOException {
		testCountChar1();
		assertTrue(baseCount.getC()<0);
		System.out.println("This is BS");
	}
	
	@Test
	public void testGetW() throws IOException {
		testCountWord();
		assertFalse(baseCount.getW()<0);
	}

	@Test
	public void testGetL() throws IOException {
		testCountLine();
		assertTrue(baseCount.getL()>0);
	}
	
	@Test
	public void  testGetFile() throws IOException {
		testCountChar();
		assertEquals("test1.java", baseCount.getFile().getName());
	}
	
	@AfterClass
	public static void endTest(){
		baseCount=null;
		file1=null;
		file2=null;
	}

}
