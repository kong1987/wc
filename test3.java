package com.kong.shoot;

import java.awt.image.BufferedImage;

public class Hero extends FlyingObject{
	private int life;//命
	private int doubleFire;//火力值
	private BufferedImage[] images;
	private int index;//获取图片数组的index
	
	public Hero() {
		// TODO Auto-generated constructor stub
		image=ShootGame.hero0;
		width=image.getWidth();
		heigth=image.getHeight();
		//x,y坐标根据游戏界面的大小确定
		x=ShootGame.WIDTH/2-50;
		y=ShootGame.HEIGTH-200;
		
		life=3;
		doubleFire=0;
		images=new BufferedImage[]{ShootGame.hero0,ShootGame.hero1};
		index=0;
		
	}

	@Override
	public void step() {
		image=images[index++/10%images.length];
		// TODO Auto-generated method stub
		
	}
	
	public Bullet[] shoot()
	{
		if(doubleFire==0)
		{
			Bullet[] bs=new Bullet[1];
			
				bs[0]=new Bullet(this.x+this.width/2, this.y-20);
		
			return bs;
		}else{
			Bullet[] bs=new Bullet[2];
			bs[0]=new Bullet(this.x+this.width/4, this.y-20);
			bs[1]=new Bullet(this.x+this.width/4*3, this.y-20);
			doubleFire-=2;
			return bs;
			
		}
		
	}
	
	
	//获取鼠标的x&y
	public void moveTo(int x,int y)
	{
		this.x=x-this.width/2;
		this.y=y-this.heigth/2;
	}

	@Override
	public boolean outOfBounds() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void addLife()
	{
		life++;
	}
	
	public int getLife()
	{
		return life;
	}
	
	public void addDoubleFire() {
		doubleFire+=40;
		
	}
	public void clearDoubleFire()
	{
		doubleFire=0;
	}
	public void subLife()
	{
		life--;
	}
	public boolean hit(FlyingObject f) {
		
		/*原方法 有误
		 * return ((f.y+f.heigth)>this.y) &&((f.y+f.heigth)<this.y+this.heigth) && 
				((f.x>this.x && f.x<this.x+this.width) 
				||(f.x+f.width>this.x &&(f.x+f.width<this.x+this.width))) ;*/
		
		int x1=f.x-this.width/2;
		int x2=f.x+this.width/2;
		int y1=f.y-this.heigth/2;
		int y2=f.x+f.heigth+this.heigth/2;
		
		int x=this.x+this.width/2;
		int y=this.y+this.heigth/2;
		
		return x>x1 && x<x2 && y>y1 && y<y2;
		
	}

}
