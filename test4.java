package com.kong.shoot;

import java.awt.image.BufferedImage;

public  abstract class FlyingObject {
	//图片坐标
	protected int x;
	protected int y;
	//图片大小
	protected int width;
	protected int heigth;
	//图片
	protected BufferedImage image;
	
	//物体移动
	public abstract void step();
	
	//检查是否越界
	public abstract boolean outOfBounds();
	
	public  boolean shootBy(Bullet bullet)
	{
		int x1=this.x;
		int x2=this.x+this.width;
		int y1=this.y;
		int y2=this.y+this.heigth;
		int x=bullet.x;
		int y=bullet.y;
		return x>x1 && x<x2 
				&&
				y>y1 && y<y2;
	}
	

}
